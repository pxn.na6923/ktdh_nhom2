import globals
import math
# from bien_doi_2D import update_rec
WIDTH = globals.WIDTH
HEIGHT = globals.HEIGHT
GRID_SIZE = globals.GRID_SIZE

def ve_truc_toa_do_2d():
    canvas = globals.canvas
    canvas.create_line(0, HEIGHT/2, WIDTH, HEIGHT/2 , fill="#454545")
    canvas.create_line(WIDTH/2, 0, WIDTH/2, HEIGHT, fill="#454545")

def ve_luoi_pixel(WIDTH, HEIGHT, GRID_SIZE):
    canvas = globals.canvas
    for i in range(int(-WIDTH/GRID_SIZE/2), int(WIDTH/GRID_SIZE/2), 1):
        canvas.create_line(i*GRID_SIZE+WIDTH/2, HEIGHT/2-300, i*GRID_SIZE+WIDTH/2, HEIGHT/2+300, fill="#a1986d", tags="luoi_pixel")
    for i in range(int(-HEIGHT/GRID_SIZE/2), int(HEIGHT/GRID_SIZE/2), 1):
        canvas.create_line(WIDTH/2-600, i*GRID_SIZE+HEIGHT/2, WIDTH/2+600, i*GRID_SIZE+HEIGHT/2, fill="#a1986d", tags="luoi_pixel")

def ve_diem(x, y, fill = "#000000", tags="diem"):
    canvas = globals.canvas
    x1, y1 = x*GRID_SIZE+ WIDTH/2 -1.5, HEIGHT/2-1.5 - y*GRID_SIZE
    x2, y2 = x*GRID_SIZE+ WIDTH/2+1.5,  HEIGHT/2+1.5 - y*GRID_SIZE
    canvas.create_rectangle(x1, y1, x2, y2, fill=fill, outline="", tags=tags)

def ve_duong_thang_bresenham( x1, y1, x2, y2, fill = "#000000", tags="duong_thang"):
    canvas = globals.canvas
    dx = abs(x2 - x1)
    dy = abs(y2 - y1)
    sx = 1 if x1 < x2 else -1
    sy = 1 if y1 < y2 else -1
    err = dx - dy
    while True:
        ve_diem(x1,y1, tags=tags, fill=fill)
        if x1 == x2 and y1 == y2:
            break
        e2 = 2 * err
        if e2 > -dy:
            err -= dy
            x1 += sx
        if e2 < dx:
            err += dx
            y1 += sy

def ve_hinh_chu_nhat( x1, y1, x2, y2, fill = "#000000", tags="hinh_chu_nhat"):
    canvas = globals.canvas
    ve_duong_thang_bresenham( x1, y1, x2, y1, fill, tags=tags)
    ve_duong_thang_bresenham( x1, y1, x1, y2, fill, tags=tags)
    ve_duong_thang_bresenham( x2, y1, x2, y2, fill, tags=tags)
    ve_duong_thang_bresenham( x1, y2, x2, y2, fill, tags=tags)
    canvas.create_rectangle(x1*5 + WIDTH/2, -y1*5 + HEIGHT/2, x2*5 + WIDTH/2, -y2*5 + HEIGHT/2, fill=fill, outline="", tags=tags)
def ve_net_dut(x1, y1, x2, y2, fill = '#000000', tags = 'net_dut'):
    canvas = globals.canvas
    dash_length = 5
    dx = abs(x2 - x1)
    dy = abs(y2 - y1)
    sx = 1 if x1 < x2 else -1
    sy = 1 if y1 < y2 else -1
    err = dx - dy

    while True:
        if dash_length >= 3:
            ve_diem(x1, y1, fill=fill, tags=tags)
        dash_length -= 1
        if x1 == x2 and y1 == y2:
            break
        e2 = 2 * err
        if e2 > -dy:
            err -= dy
            x1 += sx
        if e2 < dx:
            err += dx
            y1 += sy

        if dash_length == 0:
            dash_length = 5

def ve_ellipse_lien_dut(xc, yc, a, b, tags="elip_dut"):
    canvas = globals.canvas
    dash_length = 5
    x = 0
    y = b
    a2 = a * a
    b2 = b * b
    P = b2 - a2 * b + 0.25 * a2
    Dx =0
    Dy = 2*a2*y
    while Dx  <  Dy:
        x += 1
        Dx+= 2*b2
        if P < 0:
            P += b2+ Dx
        else:
            y -= 1
            Dy-= 2*a2
            P+= b2 + Dx- Dy
        if(dash_length>=4):
            ve_diem(xc + x, yc + y, tags=tags)
            ve_diem(xc - x, yc + y, tags=tags)
        ve_diem(xc - x, yc - y, tags=tags)
        ve_diem(xc + x, yc - y, tags=tags)
        dash_length -= 1
        if dash_length == 0:
            dash_length = 5
    P2 = round(b2 * (x + 0.5)**2 + a2 * (y - 1)**2 - a2 * b2)
    while y > 0:
        y -= 1
        Dy -= a2*2
        if P2 <= 0:
            x += 1
            Dx+= b2*2
            P2 += a2-Dy + Dx
        else:
            P2 += a2 - Dy
        if(dash_length>=4):
            ve_diem(xc + x, yc + y, tags=tags)
            ve_diem(xc - x, yc + y, tags=tags)
        ve_diem(xc + x, yc - y, tags=tags)
        ve_diem(xc - x, yc - y, tags=tags)
        dash_length -= 1
        if dash_length == 0:
            dash_length = 5
    canvas.create_oval(xc*5+ WIDTH/2 - a*5 ,-yc*5+ HEIGHT/2 - b*5 , xc*5 + WIDTH/2+ a*5 , -yc*5 + HEIGHT/2+ b *5, fill="",outline="", tags=tags)

def ve_hinh_elip(xc, yc, a, b, tags="hinh_elip", fill_diem = "#000000", fill_bg=""):
    canvas = globals.canvas
    x = 0
    y = b
    a2 = a * a
    b2 = b * b
    P = b2 - a2 * b + 0.25 * a2
    Dx =0
    Dy = 2*a2*y
    ve_diem(xc + x, yc + y, tags=tags, fill=fill_diem)
    ve_diem(xc - x, yc + y, tags=tags, fill=fill_diem)
    ve_diem(xc - x, yc - y, tags=tags, fill=fill_diem)
    ve_diem(xc + x, yc - y, tags=tags, fill=fill_diem)
    while Dx  <  Dy:
        x += 1
        Dx+= 2*b2
        if P < 0:
            P += b2+ Dx
        else:
            y -= 1
            Dy-= 2*a2
            P+= b2 + Dx- Dy
        ve_diem(xc + x, yc + y, tags=tags, fill=fill_diem)
        ve_diem(xc - x, yc + y, tags=tags, fill=fill_diem)
        ve_diem(xc + x, yc - y, tags=tags, fill=fill_diem)
        ve_diem(xc - x, yc - y, tags=tags, fill=fill_diem)
    P2 = round(b2 * (x + 0.5)**2 + a2 * (y - 1)**2 - a2 * b2)
    while y > 0:
        y -= 1
        Dy -= a2*2
        if P2 <= 0:
            x += 1
            Dx+= b2*2
            P2 += a2-Dy + Dx
        else:
            P2 += a2 - Dy
        ve_diem(xc + x, yc + y, tags=tags, fill=fill_diem)
        ve_diem(xc - x, yc + y, tags=tags, fill=fill_diem)
        ve_diem(xc + x, yc - y, tags=tags, fill=fill_diem)
        ve_diem(xc - x, yc - y, tags=tags, fill=fill_diem)
    canvas.create_oval(xc*5+ WIDTH/2 - a*5 ,-yc*5+ HEIGHT/2 - b*5 , xc*5 + WIDTH/2+ a*5 , -yc*5 + HEIGHT/2+ b *5, fill=fill_bg,outline="", tags=tags)

def ve_hinh_tron( xc, yc, R, fill = "#000000", tags="hinh_tron"):
    canvas = globals.canvas
    x = 0
    y = R
    p = 1 - R
    maxX = float(math.sqrt(2)/2*R)
    ve_diem( xc + x, yc + y, fill, tags)
    ve_diem( xc - x, yc + y, fill, tags)
    ve_diem( xc + x, yc - y, fill, tags)
    ve_diem( xc - x, yc - y, fill, tags)
    ve_diem( xc + y, yc + x, fill, tags)
    ve_diem( xc - y, yc + x, fill, tags)
    ve_diem( xc + y, yc - x, fill, tags)
    ve_diem( xc - y, yc - x, fill, tags)
    while x <= maxX:
        if p < 0:
            p += 2 * x + 3
        else:
            p += 2 * (x - y) + 5
            y -= 1
        x += 1
        ve_diem( xc + x, yc + y, fill, tags)
        ve_diem( xc - x, yc + y, fill, tags)
        ve_diem( xc + y, yc + x, fill, tags)
        ve_diem( xc - y, yc + x, fill, tags)
        ve_diem( xc + x, yc - y, fill, tags)
        ve_diem( xc - x, yc - y, fill, tags)
        ve_diem( xc + y, yc - x, fill, tags)
        ve_diem( xc - y, yc - x, fill, tags)
    canvas.create_oval(xc*5 - R*5 + WIDTH/2, -yc*5 - R*5 + HEIGHT/2, xc*5 + R*5 + WIDTH/2, -yc*5 + R *5+ HEIGHT/2, fill=fill,outline="", tags=tags)

def ve_trai_bom(xc, yc, a, b, tags="bom"):
    lb25 = globals.lb25
    lb27 = globals.lb27
    if lb25 != None and lb27 != None :
        lb25.config(text="(X:{:.1f},Y: {:.1f})".format(xc, yc))
        lb27.config(text="(a:{:.1f}, b:{:.1f})".format(a, b))
    ve_hinh_elip(xc, yc, a, b, tags=tags, fill_diem="#FF0000", fill_bg="#000000")
    ve_hinh_chu_nhat(xc +1, yc -b, xc -1, yc +b, fill="#636361", tags=tags)
    ve_hinh_chu_nhat(xc - a, yc + 1, xc + a, yc - 1, fill="#636361", tags=tags)

def ve_dia_bay( xc, yc, a, b, R, tags="dia_bay"):
    lb3 = globals.lb3
    lb5 = globals.lb5
    lb7 = globals.lb7
    lb9 = globals.lb9
    lb11 = globals.lb11
    lb13 = globals.lb13
    if lb3 != None and lb5 != None and lb7 != None and lb9 != None and lb11 != None and lb13 != None:
        lb3.config(text="(X:{:.1f},Y:{:.1f})".format(xc, yc))
        lb5.config(text="(X:{:.1f},Y:{:.1f})".format(xc, yc + b))
        lb7.config(text="(X:{:.1f},Y:{:.1f})".format(xc - a/1.5, yc + b/4))
        lb9.config(text="(X:{:.1f},Y:{:.1f})".format(xc - a/4, yc))
        lb11.config(text="(X:{:.1f},Y:{:.1f})".format(xc + a/4, yc))
        lb13.config(text="(X:{:.1f},Y:{:.1f})".format(xc + a/1.5, yc + b/4))
    ve_hinh_tron( xc, yc + b, R, fill="#FFB300" , tags=tags)
    ve_hinh_elip( xc, yc, a, b, tags=tags, fill_bg="#618ABB")
    ve_hinh_tron( xc - a/1.5, yc + b/4, R/4, fill="#FFFF00" , tags=tags)
    ve_hinh_tron( xc - a/4, yc, R/4, fill="#FFFF00" , tags=tags)
    ve_hinh_tron( xc + a/4, yc, R/4, fill="#FFFF00" , tags=tags)
    ve_hinh_tron( xc + a/1.5, yc + b/4, R/4, fill="#FFFF00" , tags=tags)

def ve_nha( x1, y1, x2, y2, tags="nha"):
    ve_hinh_chu_nhat( x1, y1, x2, y2, fill="#E9C46A", tags=tags)
    ve_hinh_chu_nhat(round(1/3*(x2+2*x1)),y1,round(1/3*(2*x2+x1)),round(1/3*(y2+2*y1)), fill="#F8DE8C", tags=tags)

def draw_tree(x1,y1,x2,y2,xc1,yc1,R, tags="cay"):
    ve_hinh_chu_nhat(x1,y1,x2,y2, fill="#8b4513", tags=tags)
    ve_hinh_tron(xc1,yc1+R*1.5,R, fill="#228b22", tags=tags)
    ve_hinh_tron(xc1+R,yc1+R,R, fill="#228b22", tags=tags)
    ve_hinh_tron(xc1-R,yc1+R,R, fill="#228b22", tags=tags)

def update_rec( x1, y1, x2, y2, x3, y3, x4, y4, tags="xe_tang"):
    lb16 = globals.lb16
    lb18 = globals.lb18
    lb20 = globals.lb20
    lb22 = globals.lb22
    if lb16 != None and lb18 != None and lb20 != None and lb22 != None:
        lb16.config(text="(X:{:.1f},Y:{:.1f})".format(x1, y1))
        lb18.config(text="(X:{:.1f},Y:{:.1f})".format(x2, y2))
        lb20.config(text="(X:{:.1f},Y:{:.1f})".format(x3, y3))
        lb22.config(text="(X:{:.1f},Y:{:.1f})".format(x4, y4))
    ve_duong_thang_bresenham(round(x1), round(y1), round(x2), round(y2), tags = tags)
    ve_duong_thang_bresenham(round(x2), round(y2), round(x3), round(y3) , tags = tags)
    ve_duong_thang_bresenham(round(x3), round(y3), round(x4), round(y4), tags = tags)
    ve_duong_thang_bresenham(round(x4), round(y4), round(x1), round(y1), tags = tags)

def ve_xe_tang(x1,y1,x2,y2,r,):
    update_rec(40, -13, 62, -13, 62, -20, 40, -19)
    ve_hinh_tron((x1+x2)/2,(y1+y2)/3,r ,fill="#658354")
    ve_hinh_chu_nhat(x1,y1,x2,y2, fill="#87ab69")
    for x in range(6):
        ve_hinh_tron(x1, y2, r/2)
        x1 += r
        
def ve_dan(xc, yc, R, tags="dan_xe_tang"):
    lb30 = globals.lb30
    if lb30 != None:
        lb30.config(text="(X:{:.1f},Y: {:.1f})".format(xc, yc))
    ve_hinh_tron(xc, yc, R, fill="#4287f5", tags=tags)
