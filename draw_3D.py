import globals
from draw_2D import ve_diem, ve_duong_thang_bresenham, ve_net_dut, ve_hinh_elip, ve_ellipse_lien_dut
import math
WIDTH = globals.WIDTH
HEIGHT = globals.HEIGHT
GRID_SIZE = globals.GRID_SIZE

def ve_truc_toa_do_3d():
    canvas = globals.canvas
    ve_duong_thang_bresenham(WIDTH/2, 0, 0, 0)
    canvas.create_text(WIDTH - 15, HEIGHT/2 + 15, text="OX", font=("Arial", 12))
    ve_duong_thang_bresenham(0, HEIGHT/2, 0, 0)
    canvas.create_text(WIDTH/2 + 15, 15, text="OY", font=("Arial", 12))
    ve_duong_thang_bresenham(round(-(math.pow(2,1/2)*HEIGHT/2)), round(-(math.pow(2,1/2)*HEIGHT/2)), 0, 0)
    canvas.create_text(285, HEIGHT - 15, text="OZ", font=("Arial", 12))

def ve_hinh_hop_chu_nhat_cabinet(x1, y1, z1, r, d, h, tags="hinh_hop_chu_nhat"):
    canvas = globals.canvas
    lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8 = globals.lb1_h, globals.lb2_h, globals.lb3_h, globals.lb4_h, globals.lb5_h, globals.lb6_h, globals.lb7_h, globals.lb8_h
    A = [x1, y1, z1]
    B = [x1 + d, y1, z1]
    C = [x1 + d, y1, z1 + r]
    D = [x1, y1, z1 + r]
    E = [x1, y1 + h, z1]
    F = [x1 + d, y1 + h, z1]
    G = [x1 + d, y1 + h, z1 + r]
    H = [x1, y1 + h, z1 + r]
    if lb1 is not None and lb2 is not None and lb3 is not None and lb4 is not None and lb5 is not None and lb6 is not None and lb7 is not None and lb8 is not None:
        lb1.config(text="(" + str(A[0]) + ", " + str(A[1]) + ", " + str(A[2]) + ")")
        lb2.config(text="(" + str(B[0]) + ", " + str(B[1]) + ", " + str(B[2]) + ")")
        lb3.config(text="(" + str(C[0]) + ", " + str(C[1]) + ", " + str(C[2]) + ")")
        lb4.config(text="(" + str(D[0]) + ", " + str(D[1]) + ", " + str(D[2]) + ")")
        lb5.config(text="(" + str(E[0]) + ", " + str(E[1]) + ", " + str(E[2]) + ")")
        lb6.config(text="(" + str(F[0]) + ", " + str(F[1]) + ", " + str(F[2]) + ")")
        lb7.config(text="(" + str(G[0]) + ", " + str(G[1]) + ", " + str(G[2]) + ")")
        lb8.config(text="(" + str(H[0]) + ", " + str(H[1]) + ", " + str(H[2]) + ")")
    points_2d = {}
    for name, point in zip(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'], [A, B, C, D, E, F, G, H]):
        x_proj = round(point[0] - math.pow(2,1/2)*point[2]/2)
        y_proj = round(point[1] - math.pow(2,1/2)*point[2]/2)
        points_2d[name] = (x_proj, y_proj)
        canvas.create_text( x_proj*GRID_SIZE + WIDTH/2 -1.5 +10 ,HEIGHT / 2 - 1.5 + 10- y_proj * GRID_SIZE , text=name, font=("Arial", 12),tags="hinh_hop_chu_nhat")
    ve_net_dut(points_2d['A'][0], points_2d['A'][1],points_2d['B'][0], points_2d['B'][1], tags=tags)
    ve_net_dut(points_2d['A'][0], points_2d['A'][1],points_2d['D'][0], points_2d['D'][1], tags=tags)
    ve_net_dut(points_2d['A'][0], points_2d['A'][1],points_2d['E'][0], points_2d['E'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['E'][0], points_2d['E'][1],points_2d['F'][0], points_2d['F'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['E'][0], points_2d['E'][1],points_2d['H'][0], points_2d['H'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['G'][0], points_2d['G'][1],points_2d['F'][0], points_2d['F'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['G'][0], points_2d['G'][1],points_2d['H'][0], points_2d['H'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['H'][0], points_2d['H'][1],points_2d['D'][0], points_2d['D'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['F'][0], points_2d['F'][1],points_2d['B'][0], points_2d['B'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['B'][0], points_2d['B'][1],points_2d['C'][0], points_2d['C'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['C'][0], points_2d['C'][1],points_2d['G'][0], points_2d['G'][1], tags=tags)
    ve_duong_thang_bresenham(points_2d['C'][0], points_2d['C'][1],points_2d['D'][0], points_2d['D'][1], tags=tags)
    
def ve_hinh_tru_cabinet(x, y, z, chieu_cao, ban_kinh1, tag='hinh_tru'):
    canvas = globals.canvas
    lb1, lb2, lb3, lb4, lb5, lb6 = globals.lb1_t, globals.lb2_t, globals.lb3_t, globals.lb4_t, globals.lb5_t, globals.lb6_t
    A = [x - ban_kinh1, y, z]
    O = [x, y, z]
    B = [x + ban_kinh1, y, z]
    C = [x - ban_kinh1, y + chieu_cao, z]
    I  = [x, y + chieu_cao, z]
    D = [x + ban_kinh1, y + chieu_cao, z]
    if lb1 is not None and lb2 is not None and lb3 is not None and lb4 is not None and lb5 is not None and lb6 is not None:
        lb1.config(text="(" + str(A[0]) + ", " + str(A[1]) + ", " + str(A[2]) + ")")
        lb2.config(text="(" + str(O[0]) + ", " + str(O[1]) + ", " + str(O[2]) + ")")
        lb3.config(text="(" + str(B[0]) + ", " + str(B[1]) + ", " + str(B[2]) + ")")
        lb4.config(text="(" + str(C[0]) + ", " + str(C[1]) + ", " + str(C[2]) + ")")
        lb5.config(text="(" + str(I[0]) + ", " + str(I[1]) + ", " + str(I[2]) + ")")
        lb6.config(text="(" + str(D[0]) + ", " + str(D[1]) + ", " + str(D[2]) + ")")
    point2d = {}
    ban_kinh2 = round(ban_kinh1* math.sqrt(2) / 2)
    for name, point in zip(['A', 'O', 'B', 'C', 'I', 'D'], [A, O, B, C, I, D]):
        x_proj = round(point[0] - math.pow(2,1/2)*point[2]/2)
        y_proj = round(point[1] - math.pow(2,1/2)*point[2]/2)
        point2d[name] = (x_proj, y_proj)
        ve_diem(x_proj, y_proj,tags=tag)
        canvas.create_text( x_proj*GRID_SIZE+ WIDTH/2 -1.5 +10 ,HEIGHT / 2 - 1.5 +10- y_proj * GRID_SIZE , text=name, font=("Arial", 12),tag="hinh_tru")
    # ve elip tren
    ve_hinh_elip(point2d['I'][0], point2d['I'][1], ban_kinh1, ban_kinh2, tags=tag)
    # # ve elip day
    ve_ellipse_lien_dut(point2d['O'][0], point2d['O'][1], ban_kinh1, ban_kinh2, tags=tag)
    # ve duong thang
    ve_duong_thang_bresenham(point2d['A'][0], point2d['A'][1], point2d['C'][0], point2d["C"][1], tags=tag)
    ve_duong_thang_bresenham(point2d['B'][0], point2d['B'][1], point2d['D'][0], point2d['D'][1], tags=tag)
    ve_duong_thang_bresenham(point2d['I'][0], point2d['I'][1], point2d['D'][0], point2d['D'][1], tags=tag)
    # ve duong khuat trong hinh tru
    ve_net_dut(point2d['I'][0], point2d['I'][1], point2d['O'][0], point2d['O'][1], tags=tag)
    ve_net_dut(point2d['B'][0], point2d['B'][1], point2d['O'][0], point2d['O'][1], tags=tag)

def get_values_and_draw():
    etr_x = globals.etr_x
    etr_y = globals.etr_y
    etr_z = globals.etr_z
    etr_d = globals.etr_d
    etr_r = globals.etr_r
    etr_h = globals.etr_h
    x = etr_x.get()
    y = etr_y.get()
    z = etr_z.get()
    dai = etr_d.get()
    rong = etr_r.get()
    cao = etr_h.get()
    ve_hinh_hop_chu_nhat_cabinet( int(x), int(y), int(z), int(dai), int(rong), int(cao))

def get_values_cylinder():
    etr_x = globals.etr_x
    etr_y = globals.etr_y
    etr_z = globals.etr_z
    etr_h = globals.etr_h
    etr_bk1 = globals.etr_bk1
    x = etr_x.get()
    y = etr_y.get()
    z = etr_z.get()
    ban_kinh1 = etr_bk1.get()
    cao = etr_h.get()
    ve_hinh_tru_cabinet( int(x), int(y), int(z), int(cao), int(ban_kinh1))

def xoa_hinh3d():
    canvas = globals.canvas
    canvas.delete("hinh_hop_chu_nhat")
    canvas.delete("hinh_tru")
