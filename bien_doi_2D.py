from draw_2D import ve_dan, ve_trai_bom, ve_dia_bay, update_rec
import numpy as np
import globals
import math

def tinh_tien(x, y, dx, dy):
    translation_matrix = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [dx, dy, 1]
    ])
    point = np.array([x, y, 1])
    new_point = np.dot(point, translation_matrix )
    return new_point[0], new_point[1]

def ve_dia_bay_tinh_tien(xc, yc, a, b, R, dx, dy, step, delay, tags="dia_bay", callback=None):
    canvas = globals.canvas
    root = globals.root
    is_paused = globals.is_pause
    distance = ((dx)**2 + (dy)**2)**0.5
    steps = int(distance / step)
    if is_paused:
        callback_id = root.after(delay, ve_dia_bay_tinh_tien, xc, yc, a, b, R, dx, dy, step, delay, tags, callback)
        globals.callback_ids.append(callback_id)
        return
    if steps == 0:
        step_dx = 0
        step_dy = 0
    else:
        step_dx = dx / steps
        step_dy = dy / steps
    if steps > 0:
        canvas.delete(tags)
        xc, yc = tinh_tien(xc, yc, step_dx, step_dy)
        ve_dia_bay(xc, yc, a, b, R, tags=tags)
        if callback:
            callback(xc, yc)
        callback_id = root.after(delay, ve_dia_bay_tinh_tien, xc, yc, a, b, R, dx - step_dx, dy - step_dy, step, delay, tags, callback)
        globals.callback_ids.append(callback_id)

def ve_dan_tinh_tien(xc, yc, R, dx, dy, step, delay, tags="dan_xe_tang", callback=None):
    canvas = globals.canvas
    root = globals.root
    distance = ((dx)**2 + (dy)**2)**0.5
    steps = int(distance / step)
    is_paused = globals.is_pause
    if is_paused:
        callback_id=root.after(delay, ve_dan_tinh_tien, xc, yc, R, dx, dy, step, delay, tags, callback)
        globals.callback_ids.append(callback_id)
        return
    if steps == 0:
        step_dx = 0
        step_dy = 0
    else:
        step_dx = dx / steps
        step_dy = dy / steps
    if steps > 0:
        canvas.delete(tags)
        xc, yc = tinh_tien(xc, yc, step_dx, step_dy)
        ve_dan(xc, yc, R, tags=tags)
        callback_id=root.after(delay, ve_dan_tinh_tien, xc, yc, R, dx - step_dx, dy - step_dy, step, delay, tags, callback)
        globals.callback_ids.append(callback_id)
    else:
        if callback:
            callback()

def scale_point(x, y, sx, sy):
    scale_matrix = np.array([
        [sx, 0, 0],
        [0, sy, 0],
        [0, 0, 1]
    ])
    point = np.array([x, y, 1])
    new_point = np.dot(scale_matrix, point)
    return new_point[0], new_point[1]

def scale_bom(xc, yc, a, b, sx, sy):
    x_goc = xc
    y_goc = yc
    xc, yc = tinh_tien(xc, yc, -xc, -yc)
    canvas = globals.canvas
    canvas.delete("bom")
    x, y = scale_point(xc+a, yc+b, sx, sy)
    a = x- xc
    b= y- yc
    xc, yc = tinh_tien(xc, yc, x_goc, y_goc)
    return xc, yc, a, b

def tinh_tien_scale_bom(xc, yc, a, b, dx, dy, step, delay, sx, sy, scale_factor, tags="bom",callback=None):
    canvas = globals.canvas
    root = globals.root
    is_paused = globals.is_pause
    distance = ((dx)**2 + (dy)**2)**0.5
    steps = int(distance / step)
    if is_paused:
        callback_id = root.after(delay, tinh_tien_scale_bom, xc, yc, a, b, dx, dy, step, delay, sx, sy, scale_factor, tags,callback)
        globals.callback_ids.append(callback_id)
        return
    if steps == 0:
        step_dx = 0
        step_dy = 0
    else:
        step_dx = dx / steps
        step_dy = dy / steps
    if steps > 0:
        canvas.delete(tags)
        xc, yc = tinh_tien(xc, yc, step_dx, step_dy)
        sx *= scale_factor
        sy *= scale_factor
        xc, yc, a_new, b_new = scale_bom(xc, yc, a, b, sx, sy)
        a_new = round(a_new)
        b_new = round(b_new)
        ve_trai_bom(xc, yc, a_new, b_new, tags=tags)
        if callback:
            callback(xc, yc)
        callback_id = root.after(delay, tinh_tien_scale_bom, xc, yc, a, b, dx - step_dx, dy - step_dy, step, delay, sx, sy, scale_factor, tags,callback)
        globals.callback_ids.append(callback_id)

def rotate_gun_barrel(start, end, step, delay, rec, pos,callback=None):
    canvas = globals.canvas
    root = globals.root
    is_pause = globals.is_pause
    if is_pause:
        callback_id=root.after(delay, rotate_gun_barrel, start, end, step, delay, rec, pos, callback)
        globals.callback_ids.append(callback_id)
        return
    if start <= end:
        canvas.delete("xe_tang")
        pt1 = rec
        pt1 = rotateObject(matrix=pt1, pos=pos, alpha=-5)
        update_rec(pt1[0][0], pt1[0][1], pt1[1][0], pt1[1][1], pt1[2][0], pt1[2][1], pt1[3][0], pt1[3][1])
        start += step
        callback_id = root.after(delay, rotate_gun_barrel, start, end, step, delay, pt1, pos, callback)
        globals.callback_ids.append(callback_id)
    else:
        if callback:
            callback()

def doi_xung_qua_Ox(x, y):
    transformation_matrix = np.array([
        [1, 0, 0],
        [0, -1, 0],
        [0, 0, 1]
    ])
    point = np.array([x, y, 1])
    new_point = np.dot(transformation_matrix, point)
    return new_point[0], new_point[1]

def doi_xung_qua_Oy(x, y):
    transformation_matrix = np.array([
        [-1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
    ])
    point = np.array([x, y, 1])
    new_point = np.dot(transformation_matrix, point)
    return new_point[0], new_point[1]

def doi_xung_qua_O(x, y):
    transformation_matrix = np.array([
        [-1, 0, 0],
        [0, -1, 0],
        [0, 0, 1]
    ])
    point = np.array([x, y, 1])
    new_point = np.dot(transformation_matrix, point)
    return new_point[0], new_point[1]

def ve_dia_bay_doi_xung(xc, yc, a, b, R, func, tags="dia_bay"):
    xc, yc = func(xc, yc)
    ve_dia_bay(xc, yc, a, b, R, tags=tags)

def convertToMatrix(x, y):
    return np.array([x, y, 1])

def convertToCoordinates(matrix):
    if len(matrix) < 3 or len(matrix) != 0:
        tmpX = matrix[0]
        tmpY = matrix[1]
        return tmpX, tmpY

def buildMatrixForRotate(matrix, alpha):
    s = math.sin(math.radians(alpha))
    c = math.cos(math.radians(alpha))
    matrix = np.array([[c,s,0], [-s,c,0], [0,0,1]])
    return matrix

def buildMatrixTranslateToOriginalPoint(x, y, matrix = []):
    matrix = np.array([[1,0,0], [0,1,0], [-x,-y,1]])
    return matrix

def multiplyMatrix(matrix1, matrix2):
    tmp = np.dot(matrix1, matrix2)
    return tmp

def rotateObject(matrix, pos, alpha):
    matrixTT1 = buildMatrixTranslateToOriginalPoint(pos[0], pos[1])
    resTT = multiplyMatrix(matrix, matrixTT1)
    matrixX = np.zeros((3,3))
    matrixX = buildMatrixForRotate(alpha=alpha, matrix=matrixX)
    resX = multiplyMatrix(resTT, matrixX)
    matrixTT2 = np.zeros((3,3))
    matrixTT2 = np.array([[1, 0, 0],[0, 1, 0],[pos[0], pos[1], 1]])
    kq = multiplyMatrix(resX, matrixTT2)
    return kq