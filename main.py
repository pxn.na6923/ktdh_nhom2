from tkinter import *
import numpy as np
import globals
import sys
from giao_dien import create_canvas, create_root, gd_chuyen_dong, gd_3D
from draw_2D import ve_dia_bay, ve_trai_bom, ve_xe_tang, ve_nha, draw_tree, ve_luoi_pixel, ve_truc_toa_do_2d
from bien_doi_2D import ve_dia_bay_tinh_tien, tinh_tien_scale_bom, rotate_gun_barrel, ve_dia_bay_doi_xung, doi_xung_qua_Oy, ve_dan_tinh_tien, doi_xung_qua_O
from draw_3D import ve_truc_toa_do_3d
from time import sleep

root = create_root()
globals.root = root
WIDTH = globals.WIDTH
HEIGHT = globals.HEIGHT
GRID_SIZE = globals.GRID_SIZE
canvas = create_canvas(root)
sys.stdout.reconfigure(encoding='utf-8')
globals.canvas = canvas

def tab2D():
    for widget in group2.winfo_children():
        widget.destroy()
    ve_hinh_2D()

def tab3D():
    for widget in group2.winfo_children():
        widget.destroy()
    ve_hinh_3D()

def ve_hinh_3D():
    canvas.delete("all")
    ve_truc_toa_do_3d()
    gd_3D(group2)

def ve_hinh_2D():
    canvas.delete("all")
    ve_bg()
    ve_truc_toa_do_2d()
    button3 = Button(group2, text="Vẽ/Xóa Lưới Pixel", command=toggle_luoi_pixel, bg="green", fg="orange", width=26, height=1,font=("Arial", 12))
    button3.grid(row=0, column=0, padx=30, pady=10, columnspan=4)
    button4 = Button(group2, text="Vẽ Vật", command=ve_vat, bg="green", fg="orange", width=26, height=1,font=("Arial", 12))
    button4.grid(row=1, column=0, padx=30, pady=10, columnspan=4)
    button5 = Button(group2, text="Chuyển Động", command=chuyen_dong, bg="green", fg="orange", width=26, height=1,font=("Arial", 12))
    button5.grid(row=2, column=0, padx=30, pady=10, columnspan=4)

def ve_bg():
    canvas.create_rectangle(0, 0, WIDTH, HEIGHT/2.5, fill="#AACAC5", outline="")
    canvas.create_rectangle(0, HEIGHT/2.5, WIDTH, HEIGHT, fill="#BDAF7F", outline="")

def xoa_luoi_pixel():
    canvas.delete("luoi_pixel")

luoi_da_ve = False

def toggle_luoi_pixel():
    global luoi_da_ve
    if luoi_da_ve == True:
        xoa_luoi_pixel()
        luoi_da_ve = False
    else:
        ve_luoi_pixel(WIDTH, HEIGHT, GRID_SIZE)
        ve_truc_toa_do_2d()
        luoi_da_ve = True

def ve_vat():
    canvas.delete("all")
    globals.is_pause = True
    stop_motion()
    ve_bg()
    ve_truc_toa_do_2d()
    ve_dia_bay(-60, 30, 22, 6, 7)
    ve_trai_bom(-60, 15, 5, 5)
    ve_xe_tang( 40, -20, 80, -40, 8)
    ve_nha( -57, -44, -37, -20)
    draw_tree(-3,-22,3,-44,0,-33,5)
    draw_tree(-110,-22,-104,-44,-107,-33,5)
    draw_tree(104,-22,110,-44,107,-33,5)

def bd_bom(xc, yc):
    if(yc==-30):
        sleep(0.5)
        canvas.delete("nha")
        canvas.delete("bom")

def bd_dia_bay(xc, yc):
        if(xc==60):
            ve_dia_bay_doi_xung(60, 40, 22, 6, 7, doi_xung_qua_Oy)
            ve_dia_bay_tinh_tien(-60, 40, 22, 6, 7, 120, 0, 5, 400, callback=bd_dia_bay1)
            tinh_tien_scale_bom(-60, 30, 5, 5, 0, -60, 5, 300, 1, 1, 1.1, tags="bom", callback=bd_bom)

def bd_bom1(xc, yc):
    if(yc==-30):
        sleep(0.5)
        canvas.delete("cay1")
        canvas.delete("bom1")
        
def bd_dia_bay1(xc, yc):
    if(xc == 0):
        canvas.delete("nha")
        tinh_tien_scale_bom(xc, 30, 5, 5, 0, -60, 5, 300, 1, 1, 1.1, tags="bom1", callback=bd_bom1)
    if(xc==60):
        ve_dia_bay(60, 40, 22, 6, 7, tags="dia_bay")
        ve_dia_bay_doi_xung(60, 40, 22, 6, 7, doi_xung_qua_O, tags="dia_bay1")
        canvas.delete("dia_bay")
        globals.is_pause = True

def bd_xoay_done():
    ve_dan_tinh_tien(60, 5, 2, 0, 60, 5, 300, callback=bd_xoay_done1)

def bd_xoay_done1():
    ve_dan_tinh_tien(60, 5, 2, 0, 60, 5, 300, callback=bd_xoay_done2)

def bd_xoay_done2():
    ve_dan_tinh_tien(60, 5, 2, 0, 60, 5, 300)

def stop_motion():
    root = globals.root
    for callback_id in globals.callback_ids:
        root.after_cancel(callback_id)
    globals.callback_ids.clear()
    globals.lb3 , globals.lb5, globals.lb7, globals.lb9, globals.lb11, globals.lb13, globals.lb18, globals.lb16, globals.lb20, globals.lb22, globals.lb25, globals.lb27, globals.lb30 = None, None, None, None, None, None, None, None, None, None, None, None, None
    
def chuyen_dong():
    canvas.delete("all")
    globals.is_pause = False
    stop_motion()
    lb3 , lb5, lb7, lb9, lb11, lb13, lb18, lb16, lb20, lb22, lb25, lb27, lb30= gd_chuyen_dong(group2)
    globals.lb3 , globals.lb5, globals.lb7, globals.lb9, globals.lb11, globals.lb13, globals.lb18, globals.lb16, globals.lb20, globals.lb22, globals.lb25, globals.lb27, globals.lb30 = lb3 , lb5, lb7, lb9, lb11, lb13, lb18, lb16, lb20, lb22, lb25, lb27, lb30
    ve_bg()
    ve_truc_toa_do_2d()
    ve_dia_bay_tinh_tien(120, 40, 22, 6, 7, -60, 0, 5, 400, callback=bd_dia_bay)
    rotate_gun_barrel(0, 17, 1, 200, rec, pos, callback=bd_xoay_done)
    ve_xe_tang( 40, -20, 80, -40, 8)
    ve_nha( -57, -44, -37, -20, tags="nha")
    draw_tree(-3,-22,3,-44,0,-33,5, tags="cay1")
    draw_tree(-110,-22,-104,-44,-107,-33,5, tags="cay2")
    draw_tree(104,-22,110,-44,107,-33,5, tags="cay3")

group1 = LabelFrame(root)
group1.grid(row=0, column=0, padx=10, pady=10, sticky=E+W+N+S)
button1 = Button(group1, text="Hình Vẽ 2D", command=tab2D, bg="green", fg="orange", width=10, height=1,font=("Arial", 16))
button1.grid(row=0, column=0, padx=10, pady=10)
button2 = Button(group1, text="Hình Vẽ 3D", command=tab3D, bg="green", fg="orange", width=10, height=1,font=("Arial", 16))
button2.grid(row=0, column=1,rowspan=3, padx=10, pady=10)
group2 = LabelFrame(root)
group2.grid(row=0, column=1,rowspan=5, padx=10, pady=10, sticky=E+W+N+S)
button3 = Button(group2, text="Vẽ/Xóa Lưới Pixel", command=toggle_luoi_pixel, bg="green", fg="orange", width=26, height=1,font=("Arial", 12))
button3.grid(row=0, column=0, padx=30, pady=10, columnspan=4)
button4 = Button(group2, text="Vẽ Vật", command=ve_vat, bg="green", fg="orange", width=26, height=1,font=("Arial", 12))
button4.grid(row=1, column=0, padx=30, pady=10, columnspan=4)
button5 = Button(group2, text="Chuyển Động", command=chuyen_dong, bg="green", fg="orange", width=26, height=1,font=("Arial", 12))
button5.grid(row=2, column=0, padx=30, pady=10, columnspan=4)

ve_bg()
ve_truc_toa_do_2d()
pos = np.array([60, -18])
rec = np.array([[40,-15,1],[62,-15,1], [62,-20,1],[40,-20,1]])

def exit_program():
    root.quit()
root.protocol("WM_DELETE_WINDOW", exit_program)
root.mainloop()